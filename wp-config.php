<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'cursos_bd' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '.sTA095Xb?wF.XBtX#&zCi3iC6R.^GhOor5`)knX4m7.mk6lF{kqq7o@y+XMrB18' );
define( 'SECURE_AUTH_KEY',  'eo{}y?6r 7az&X]yq5+>+<s`6Ul?QG%eo  z]3:DjcQ*;)u=[t W-s![P9`|mR{q' );
define( 'LOGGED_IN_KEY',    'Sr]%,#z7)3CY,gzs{wnaBhFkcX)_9CK69%m:7hr$mBDC/|8{p7G->zxNk(N&O$mb' );
define( 'NONCE_KEY',        'lz:i}pDCw:.QBcFFo(o4M!)nlV#fbsA<m0j7/+ddK}-@Yy&t]%Y`5e0<@_dA&.+D' );
define( 'AUTH_SALT',        'M]E[HEEON]b)*37JYUCqaegs(TejQIsTPmWdnZ/{ris1BQaG5A3*OLDcr%7Gb42B' );
define( 'SECURE_AUTH_SALT', 'Q%A6eV2,<5Ok>&mf!>VH3:HB3e*)xxve&3-R.Tl!!AFv(>WtJXe31l;p{?J9[:V?' );
define( 'LOGGED_IN_SALT',   'xP4.CZazG?EzIwVptEt#{;wM!uRy%7cTO85Sz@>WM2s/vh9/.o`3Sn._J4Db]%[u' );
define( 'NONCE_SALT',       'Z-,hWV-8GFPnq)%uNL9$mn.Zwi:jxF_Y(FuMYr:aXV/X.X-qEJL[10=iAHWlb^UW' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
