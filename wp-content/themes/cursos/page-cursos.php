<?php
/* 
*  Template Name: Cursos
*
*/ ?>

<?php get_header(); ?>

<div class="contenedor">
    <section class='cursos'>
        <div class='caja'>
            <p class='icon'>H</p>
            <h2>Desarrollo web</h2>
            <p>Aprende los principales lenguajes para desarrollar webs ¡Conviértete en un web máster!</p>
        </div>

        <div class='caja'>
            <p class='icon'>_</p>
            <h2>Sistemas operativos</h2>
            <p>Aprende a administrar sistemas linux y windows</p>
        </div>

        <div class='caja'>
            <p class='icon'>S</p>
            <h2>Harware</h2>
            <p>Conoce los entresijos del hardware</p>
        </div>

        <div class='caja'>
            <p class='icon'>u</p>
            <h2>Redes e internet</h2>
            <p>Aprende configurar y administrar redes informáticas y servidores</p>
        </div>

        <div class='caja'>
            <p class='icon'>a</p>
            <h2>Bases de datos</h2>
            <p>Aprende montar y administrar bases de datos</p>
        </div>

        <div class="clearfix"></div>
    </section>
</div>

<?php get_footer(); ?>