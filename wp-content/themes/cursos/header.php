<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0"/>

	<?php wp_head(); ?>

	<title>Proyecto Blog</title>
</head>

<body class='preload'>

	<header id='header'>
		<div class="cont-nav">
			<a href="<?php echo home_url(); ?>" style="color: white;">
				<div class="logo">
					<span class='gear'>S</span>
					<h3>home</h3>
				</div>
			</a>

			<!--
			<nav id='menu'>
				<ul>
					<li> <a href="#">inicio</a> </li>
					<li> <a href="#">blog</a> </li>
					<li> <a href="#">formación</a> </li>
					<li> <a href="#">contacto</a> </li>
				</ul>
			</nav> -->

			<?php
            wp_nav_menu(
                array(
                    'theme_location' => 'menu-principal',
                    'container' => 'nav',
                    'container_class' => '',
                    'container_id' => 'menu',
                    'items_wrap' => '<ul>%3$s</ul>',
                    'menu_class' => 'nav-item'
                )
            ); ?>

			<div class="clearfix"></div>
			
		</div>
	</header>