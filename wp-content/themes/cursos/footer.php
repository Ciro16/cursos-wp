<footer id='footer'>
		<div class="contenedor">
			<div id='menu_footer'>
				<h5>MENU</h5>
				<ul>
					<li> <a href="#">inicio</a> </li>
					<li> <a href="otravaina.html">blog</a> </li>
					<li> <a href="#">formación</a> </li>
					<li> <a href="#">contacto</a> </li>
				</ul>
			</div>

			<div class="location">
				<h5>¿DÓNDE ESTAMOS?</h5>
				<iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d945.9680175954384!2d-69.8605116708449!3d18.489453071094093!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8eaf89ae0dd15e17%3A0x6b1114e547fb3f5e!2sCondominio%20GINI%20VIII!5e0!3m2!1ses!2sdo!4v1596421100078!5m2!1ses!2sdo" 
				frameborder="0" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
			</div>

			<div id="info">
				<h5>Desarrollado con</h5>
				<div class="logos-lenguaje">
					<p class="img-html"><img class="logo-lenguaje html" src="<?php echo get_template_directory_uri() . '/img/html5.png' ?>" alt="Imagen HTML5"></p>
					<p class="img-html"><img class="logo-lenguaje css" src="<?php echo get_template_directory_uri() . '/img/css3.png' ?>" alt="Imagen HTML5"></p>
					<p class="img-html"><img class="logo-lenguaje php" src="<?php echo get_template_directory_uri() . '/img/php.png' ?>" alt="Imagen HTML5"></p>
					<p class="img-html"><img class="logo-lenguaje wordpress" src="<?php echo get_template_directory_uri() . '/img/wordpress.png' ?>" alt="Imagen HTML5"></p>
				</div>

				<div class="clearfix"></div>

				<h5>Optimizado para</h5>
				<p id="browsers">
					<a href="">
						<img src="<?php echo get_template_directory_uri() . '/img/chrome.png' ?>" alt="chrome">
					</a>
					<a href="">
						<img src="<?php echo get_template_directory_uri() . '/img/firefox.png' ?>" alt="firefox">
					</a>
					<a href="">
						<img src="<?php echo get_template_directory_uri() . '/img/opera.png' ?>" alt="opera">
					</a>
					<a href="">
						<img src="<?php echo get_template_directory_uri() . '/img/safari.png' ?>" alt="safari">
					</a>
				</p>
			</div>

		</div>
	</footer>
	
	
</body>
</html>
