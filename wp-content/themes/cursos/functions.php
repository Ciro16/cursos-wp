
<?php

//Estilos y JavaScripts
function agregar_css_js(){
    wp_enqueue_style('style', get_stylesheet_uri());    
}
add_action('wp_enqueue_scripts', 'agregar_css_js');

//Menu de navegación
function registrando_menus() {
    register_nav_menus(
        array(
            'menu-principal' => __( 'Menu Principal' )
        )
    );
}
add_action( 'init', 'registrando_menus' );

// Removiendo &nbsp; del contenido
function replace_content($content) {
    $content = htmlentities($content, null, 'utf-8');
    $content = str_replace("&nbsp;", " ", $content);
    $content = html_entity_decode($content);
    return $content;
}
add_filter('the_content','replace_content', 999999999);

//Modificando el numero limite de palabras en the_excerpt()
function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

// Modificando el link para leer el contenido completo
function wpdocs_excerpt_more( $more ) {
    if ( ! is_single() ) {
        $more = sprintf( '<a href="%1$s">%2$s</a>',
            get_permalink( get_the_ID() ),
            __( 'Leer más...', 'textdomain' )
        );
    }
 
    return $more;
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

// Agregando soporte para imagenes de los Post's
add_theme_support( 'post-thumbnails' );

if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 500, 500, true ); // default Featured Image dimensions (cropped)
    
 }