
<?php get_header(); ?>

	<div class="portada">

		<section class='info'>
			<div class="banner">
				<h1>cursos de desarrollo web con ciro pérez</h1>
			</div>	
		</section>

		<h2 class="title">¿listo para empezar?</h2>
	</div>

	<div class="contenedor">
    	<div class="cont-principal">
        	<div class="blog">
				<div class='ult-articulos fondo'>
					<h3>ultimos articulos</h3>
				</div>

				<?php if( have_posts() ): ?>
				<?php 	while( have_posts() ): ?>
				<?php 		the_post(); ?>
							<article>
								<h3> <?php the_title(); ?></h3>
								<?php the_excerpt(); ?>

								<div class="info">Fecha: <?php the_time('F j, Y'); ?></div>
							</article>

				<?php 	endwhile;?>
				<?php endif; ?>

			</div>

    	</div>
	</div>

<?php get_footer(); ?>