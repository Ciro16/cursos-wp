
<?php get_header(); ?>

<div class="contenedor">

    <?php the_post(); ?>

    <div class="cont-post">
        <div class="imagen-post-contenedor">
            <!-- <img class="imagen-post" src="<?php echo get_template_directory_uri() . '/img/portada.jpg'; ?>" alt=""> -->

            <?php if(has_post_thumbnail()){
                the_post_thumbnail();
            }
            ?>
                
        </div>

        <div class="contenido-post">
            <h3><?php the_title(); ?></h3>

            <?php the_content(); ?>
        </div>

    </div>
</div>

<?php get_footer(); ?>